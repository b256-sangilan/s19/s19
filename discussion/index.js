// console.log("Happy Tuesday!");

// What are conditional Statements?

	//Conditional statements allow us to control the flow of our program. It allows us to run a statement/instruction if a condition is met or run another separate instruction if otherwise.

// [SECTION] if, else if a specific statement

let numA = -1;

// if statement
	// executes a statement if a specific condition is true

// It will not run because the condition  result to false

console.log(numA < 0);

if(numA < 0) {
	console.log("numA is less than 0");
}
console.log(numA < 0);

// else if Clause
/* 
    - Executes a statement if previous conditions are false and if the specified condition is true
    - The "else if" clause is optional and can be added to capture additional conditions to change the flow of a program

*/

let numB = 1;

if(numB < 0) {
	console.log("numB is less than 0");
}
else if(numB > 0) {
	console.log("numB is greater than 0");
};

let city = "Tokyo";

if(city === "New York") {
	console.log("Welcome to New York City");
}
else if(city === "Tokyo") {
	console.log("Welcome to Tokyo");
};

// else Statement
/* 
    - Executes a statement if all other conditions are false
    - The "else" statement is optional and can be added to capture any other result to change the flow of a program
*/

let numC = 0;

if(numC > 0) {
	console.log("Hello");
} else if (numC < 0) {
	console.log("World");
} else {
	console.log("Again"); // it executes the statement because the first two conditions are false
};

// Note: we cannot create a conditional statement without a preceeding if() statement.

let greeting = "Hola";

if(greeting === "Mabuhay") {
	console.log("Mabuhay!")
} else if(greeting === "Hola") {
	console.log("Hola!");
} else if(greeting === "Konichiwa") {
	console.log("Konnichiwa");
} else {
	console.log("Hello");
};

// [SECTION] if, else if, and else Statements with Functions

/*
    - Most of the times we would like to use if, else if and else statements with functions to control the flow of our application
    - By including them inside functions, we can decide when certain conditions will be checked instead of executing statements when the JavaScript loads
    - The "return" statement can be utilized with conditional statements in combination with functions to change values to be used for other features of our application
*/

let message = "No message";
console.log(message);

function determineTyphoonIntensity(windSpeed) {
	if(windSpeed < 30) {
		return "Not a typhoon yet.";
	} 
	else if(windSpeed <= 61) {
		return 'Tropical Depression Detected';
	} 
	else if(windSpeed >= 62 && windSpeed <= 88) {
		return 'Tropical Storm Detected';
	} 
	else if(windSpeed >= 89 || windSpeed <= 117) {
		return 'Severe Tropical Storm Detected';
	}
	else {
		return 'Typhoon Detected';
	}
}
message = determineTyphoonIntensity(110);
console.log(message);

/* 
    - We can further control the flow of our program based on conditions and changing variables and results
    - The initial value of "message" was "No message."
    - Due to the conditional statements created in the function, we were able to reassign it's value and use it's new value to print a different output
    - console.warn() is a good way to print warnings in our console that could help us developers act on certain output within our code
*/

if(message == 'Severe Tropical Storm Detected') {
	console.warn(message);
}

// Returns the string to the variable "message" that invoked it
message = determineTyphoonIntensity(110);
console.log(message);

	console.warn(message);


// [SECTION] Truthy and Falsy
/*
	In JavaScript a 'truthy' value is a value that is considered true when encountered in a Boolean context
	- Values are considered true unless defined otherwise
		- falsy values/exceptions for trutht:
			1. false
			2. 0
			3. -0
			4. ""
			5. null
			6. undefined
			7. NaN
 */
// Truthy examples
/*
	- If the rsult of an expression in a condition results to a truthy value, the condition returns true and the corresponding statement are excute
	- expressions are any unit of core that can be evaluated to a value

 */
if (true){
	console.log('Truthy');
}

if (1){
	console.log('Truthy');
}

if ([]){
	console.log('Truthy');
}

// Falsy Examples
if (false) {
	console.log('Falsy');
}

if (0) {
	console.log('Falsy');
}

if (undefined) {
	console.log('Falsy');
}

// [SECTION] Conditional (Ternary) Operator
/*
	- The conditional (Ternary) operator takes in three operands
		1. condition
		2. expression to execute if the condition is truthy
		3. expression to execute if the condition is falsy
	- can be used as an alternative to an 'if else' statement
	- ternary operators have an implicit 'return' statement meaning that without the 'return' keyword, the resulting expression can be stored variable
	- commonly used for single statement expression where the result consist of only one line of code
	- for multiple lines of code/blocks, a function may be defined then used in a ternary operator
		- syntax:
			(expression) ? ifTrue : ifFalse;
 */
// Single statement execution
let ternaryResult = (1 < 18) ? true : false;
console.log("Result of Ternary Operator: " + ternaryResult);

// Multiple Statement Execution

let name;

function isOfLegalAge(){
	name = 'Jungkook';
	return 'You are of the Legal age limit';
}

function isUnderAge(){
	name = 'Jane';
	return 'You are under the age limit';
}


/*
   - the prompt function crates a pop-up mesage in the browser that can be used to gather user input 
   - input received from the prompt function is returned as string data type
   - the "parseInt" function converts the input received into a number data type
   - pressing on the "cancel" button on prompt will return a value of null 
   - converting null to an integer/number will result to a NaN (Not a Number) value
 */

let age = parseInt(prompt("What is your age? "))
console.log(age);
let legalAge = (age > 18) ? isOfLegalAge(): isUnderAge();
console.log("Result of Ternary Operator in Function: " + legalAge + ' ' + name);
/*
 if (age > 18) {
	isOfLegalAge();
}

else (age < 18) {
	isUnderAge(); is the same as 

let legalAge = (age > 18) ? isOfLegalAge(): isUnderAge();
console.log("Result of Ternary Operator in Function: " + legalAge + ' ' + name)

The answer is NO, the two code block are not the same.

The first code block is an if-else statement that checks whether the age is greater than 18, if it is 'true', it calls the function isOfLegalAge(). Otherwise, it calls the function isUnderAge.

The second code block is a ternary operator that uses a conditional expression to set the value of the legalAge variable based on the age condition. If the age is grater than 18, it sets the value to the result of the isOfLegalAge() function. Otherwise, it sets the value to the result of the isUnderage() function.

Also, the first code block has a syntax error because the else statement should not have a condition
 */

// [SECTION] Switch Statement
/*
 	- the switch statement evaluates an expression and matches the expression's value to a case clause. The switch will execute the statements associated with that case, as well as statements in cases that follow the matching case.
 	- can be used as an alternative to an if, "else if and else" statement where the data to be used in the condition is of an expected output
 	- syntax:
 		switch (expression){
			case value:
				statement;
				break;
			default: 
				statement;
				break
 		}
 	- expression is the information used to match the 'value' provided in the switch cases
 	- variables are commonly used as expressions to allow varying user inpur to be used when comparing switch case values
 	- break statement is used to terminate the current loop once a match has been found
 	- 

 */

// toLowerCase function/method will change the input received from prompt into all lowercase letter
day = prompt("What day of the week is it today? ").toLowerCase();
console.log(day);

switch (day){
	case 'monday': 
		console.log("The color of the day is red.");
		break;
	case 'tuesday': 
		console.log("The color of the day is orange.");
		break;
	case 'wednesday': 
		console.log("The color of the day is yellow.");
		break;
	case 'thursday': 
		console.log("The color of the day is green.");
		break;
	case 'friday': 
		console.log("The color of the day is blue.");
		break;
	default:
		console.log("Please input a valid day.")
		break;
}

// [SECTION] Try-Catch-Finally Statement
/*
	- "try catch" statements are commonly used for error handling
	- there are instance when the application returns an error/warning that is not necessarily an error in the context of our code
	- these are errors are a result of an attempt of the programming language to help developers in creating efficient code
	- they are used to specify a response whenever an exception/error is received

 */

function showIntensityAlert(windSpeed){
	try {
		alert(determineTyphoonIntensity(windSpeed));
	//error/err are commonly used variable names used by the developers for storing errors 
	} catch(err) {
		// typeof operator is used to check the data type of a value/expression and return a string value or waht the data type is
		console.log(typeof err);

		// Catch error within 'try' statement
		// in this case the error is an unknown function 'alerat' which does not exist in JavaScript
		// alert function is used similarly to a prompt to alert the user
		// err.message is used to access the information relating to an error object 
		console.warn(err.message)
	} finally{
		alert("Intensity updates will show new alert.")
	}
}
showIntensityAlert(5);